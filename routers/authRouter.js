const express = require("express");
const router = express.Router();
const auth = require("../controllers/UserManagement/auth");
const logged = require("../middlewares/logged");

router.get("/login", logged, auth.viewLogin);
router.get("/signup", logged, auth.viewSignUp);
router.get("/admin", auth.viewAdmin);
router.post("/login", auth.login);
router.post("/signup", auth.signUp);
router.post("/admin", auth.admin);

module.exports = router;
