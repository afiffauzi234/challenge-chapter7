const express = require("express");
const router = express.Router();
const apiAuth = require("../controllers/UserManagement/apiauth");

router.post("/api/login", apiAuth.apiLogin);
router.post("/api/signup", apiAuth.apiSignUp);
router.post("/api/logout", apiAuth.apiLogout);

module.exports = router;
