const express = require("express");
const router = express.Router();
const dashboard = require("../controllers/UserManagement/dashboard");

router.get("/", dashboard.viewDashboard);
router.get("/create", dashboard.viewCreate);
router.get("/edit/:id", dashboard.viewEdit);
router.post("/create", dashboard.create);
router.post("/edit/:id", dashboard.edit);
router.post("/:id", dashboard.delete);
router.post("/", dashboard.logout);

module.exports = router;
