const express = require("express");
const router = express.Router();
const game = require("../controllers/UserManagement/game");
const restrictJwt = require("../middlewares/restrict-jwt");

router.post("/api/create-room", restrictJwt, game.createRoom);
router.post("/api/fight/:id", restrictJwt, game.playGame);

module.exports = router;
