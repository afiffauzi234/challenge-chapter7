const express = require("express");
const router = express.Router();
const dashboardRouter = require("./dashboardRouter");
const authRouter = require("./authRouter");
const gameRouter = require("./gameRouter");
const apiAuthRouter = require("./apiAuthRouter");
const restrict = require("../middlewares/restrict");

router.use(express.static("public"));

router.get("/", (req, res) => {
  res.render("index");
});

router.get("/game", (req, res) => {
  res.render("gamevscom");
});

router.use("/", authRouter);
router.use("/", apiAuthRouter);
router.use("/", gameRouter);
router.use("/dashboard", restrict, dashboardRouter);

router.use("/", (req, res, next) => {
  res.status(404);
  res.json({
    status: "404",
    errors: "page not found",
  });
});

module.exports = router;
