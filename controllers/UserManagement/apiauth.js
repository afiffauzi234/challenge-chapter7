const { UserGames, UserGameBiodata, UserGameHistories } = require("../../models");

function format(user) {
  const { id, username } = user;
  return {
    id,
    username,
    accessToken: user.generateToken(),
  };
}

module.exports = {
  apiSignUp: async (req, res) => {
    try {
      const { username, password, fullname, age } = req.body;
      const user = await UserGames.register({ username, password });
      await UserGameBiodata.create({
        fullname,
        age,
        user_id: user.dataValues.id,
      });
      await UserGameHistories.create({
        score: 0,
        user_id: user.dataValues.id,
      }).then((user) => {
        res.json(user);
      });
    } catch (err) {
      res.json({ error: err.message });
    }
  },

  apiLogin: async (req, res) => {
    try {
      const user = await UserGames.authenticate(req.body);
      const { id, username } = user;
      res.json({
        id,
        username,
        accessToken: user.generateToken(),
      });
    } catch (error) {
      console.error(error);
    }
  },

  apiLogout: async (req, res) => {
    try {
      req.logout();
      res.redirect("/");
    } catch (error) {
      console.log(error);
    }
  },
};
