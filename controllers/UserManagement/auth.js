const passport = require("../../lib/passport");
const { UserGames, UserGameBiodata, UserGameHistories } = require("../../models");
const admin = require("../../database/user.json");

module.exports = {
  viewLogin: async (req, res) => {
    try {
      res.render("login");
    } catch (error) {
      console.log(error);
    }
  },

  login: passport.authenticate("local", {
    successRedirect: "/dashboard",
    failureRedirect: "/login",
    failureFlash: true, // Untuk mengaktifkan express flash
  }),

  viewSignUp: async (req, res) => {
    try {
      res.render("signup");
    } catch (error) {
      console.log(error);
    }
  },

  signUp: async (req, res) => {
    try {
      const { username, password, fullname, age } = req.body;
      const user = await UserGames.register({ username, password });
      await UserGameBiodata.create({
        fullname,
        age,
        user_id: user.dataValues.id,
      });
      await UserGameHistories.create({
        score: 0,
        user_id: user.dataValues.id,
      }).then((users) => {
        res.redirect("/login");
        console.log(users);
      });
    } catch (error) {
      console.log(error);
    }
  },

  viewAdmin: async (req, res) => {
    try {
      res.render("admin");
    } catch (error) {
      console.log(error);
    }
  },

  admin: passport.authenticate("local", {
    successRedirect: "/dashboard",
    failureRedirect: "/admin",
    failureFlash: true, // Untuk mengaktifkan express flash
  }),
};
