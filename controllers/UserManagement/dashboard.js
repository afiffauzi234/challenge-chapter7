const { UserGames, UserGameBiodata, UserGameHistories } = require("../../models");

UserGames.hasOne(UserGameBiodata, { foreignKey: "user_id", as: "UserGameBiodata" });
UserGames.hasOne(UserGameHistories, { foreignKey: "user_id", as: "UserGameHistories" });

module.exports = {
  viewDashboard: async (req, res) => {
    try {
      await UserGames.findAll({
        order: [["id", "ASC"]],
        include: ["UserGameBiodata", "UserGameHistories"],
      }).then((users) => {
        res.render("dashboard", { users });
        console.log(users);
      });
    } catch (error) {
      console.log(error);
    }
  },

  viewCreate: async (req, res) => {
    try {
      res.render("create");
    } catch (error) {
      console.log(error);
    }
  },

  create: async (req, res) => {
    try {
      const { username, password, fullname, age } = req.body;
      const user = await UserGames.register({ username, password });
      await UserGameBiodata.create({
        fullname,
        age,
        user_id: user.dataValues.id,
      });
      await UserGameHistories.create({
        score: 0,
        user_id: user.dataValues.id,
      }).then((users) => {
        res.redirect("/dashboard");
        console.log(users);
      });
    } catch (error) {
      console.log(error);
    }
  },

  viewEdit: async (req, res) => {
    try {
      const user = await UserGames.findOne({
        where: { id: req.params.id },
      });
      const biodata = await UserGameBiodata.findOne({
        where: { user_id: req.params.id },
      });
      res.render("edit", { user, biodata });
      console.log(user, biodata);
    } catch (error) {
      console.log(error);
    }
  },

  edit: async (req, res) => {
    try {
      const { username, fullname, age } = req.body;
      await UserGames.update(
        {
          username,
        },
        {
          where: {
            id: req.params.id,
          },
        }
      );
      await UserGameBiodata.update(
        {
          fullname,
          age,
        },
        {
          where: {
            user_id: req.params.id,
          },
        }
      ).then((users) => {
        res.redirect("/dashboard");
        console.log(users);
      });
    } catch (error) {
      console.log(error);
    }
  },

  delete: async (req, res) => {
    try {
      await UserGames.destroy({
        where: {
          id: req.params.id,
        },
      });
      await UserGameBiodata.destroy({
        where: {
          user_id: req.params.id,
        },
      });
      await UserGameHistories.destroy({
        where: {
          user_id: req.params.id,
        },
      }).then((users) => {
        res.redirect("/dashboard");
        console.log(users);
      });
    } catch (error) {
      console.log(error);
    }
  },

  logout: async (req, res) => {
    try {
      req.logout();
      res.redirect("/");
    } catch (error) {
      console.log(error);
    }
  },
};
