const wait = {};

const data = {};

const { GamePlay } = require("../../models/gameplay");

function waitEnemy(id) {
  return new Promise((resolve) => {
    wait[id] = { resolve };
  });
}

module.exports = {
  createRoom: (req, res) => {
    res.json({ id: 1, name: req.body.name });
  },

  playGame: async (req, res) => {
    const id = req.params.id;

    // const play = await GamePlay.findOne({
    //     player2: null,
    //     room_id: id
    // })
    // if (!play) {
    //     await GamePlay.create({
    //         player1: req.body.choose,
    //         player2: null,
    //         room_id: id,
    //     })
    // } else {
    //     await GamePlay.update({
    //         player2: req.body.choose
    //     },
    //     {
    //         where : { room_id: id }
    //     }),
    // }

    if (!data[id]) {
      // Player 1 memilih
      data[id] = {
        player1: req.body.choose,
        player2: null,
      };
    } else {
      // Player 2 memilih
      data[id].player2 = req.body.choose;
    }

    if (!wait[id]) {
      // Player 1 menunggu respons player 2
      await waitEnemy(id);
    } else {
      // Player 2 merespons ke player 1
      wait[id].resolve();
      delete wait[id];
    }

    res.json(data[id]);
  },
};
