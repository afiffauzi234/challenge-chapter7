// Parent Class - ABSTRACTION
class Player {
  constructor(props) {
    let { name, pick, index } = props;
    this.name = name;
    this.pick = pick;
    this.index = index;
  }

  identify() {
    console.log("Player is ?");
  }

  #cheating() {
    console.log(`${this.name} Cheating`); // ENCAPSULATION
  }
}

// Child Class - INHERITANCE
class Human extends Player {
  constructor(props) {
    super(props);
  }

  identify() {
    super.identify();
    console.log("Player 1 is Human"); //POLYMORPHISM with Overriding
  }
}

class Computer extends Player {
  constructor(props) {
    super(props);
  }

  identify() {
    super.identify();
    console.log("Player 2 is Computer"); //POLYMORPHISM with Overriding
  }
}

// Class Game
class Game {
  constructor(props) {
    let { result, player1, player2 } = props;
    this.result = result;
    this.player1 = player1;
    this.player2 = player2;
  }

  startGame() {
    if (computer.pick[0].style.backgroundColor == "silver" || computer.pick[1].style.backgroundColor == "silver" || computer.pick[2].style.backgroundColor == "silver") {
      for (let i = 0; i < 3; i++) {
        player1.pick[i].style.backgroundColor = "#9c835f";
        computer.pick[i].style.backgroundColor = "#9c835f";
      }
      this.result.innerHTML = `<p class="vs">VS</p>`;
      pickStatus = false;
    } else {
      let timesRun = 0;
      let indexBefore = 0;
      let intervalGenerator = setInterval(() => {
        this.player2.index = Math.floor(Math.random() * 3);
        this.player2.pick[this.player2.index].style.backgroundColor = "silver";
        if (timesRun > 0) {
          this.player2.pick[indexBefore].style.backgroundColor = "#9c835f";
        }
        indexBefore = this.player2.index;
        timesRun += 1;
        if (timesRun == 2) {
          clearInterval(intervalGenerator);
          this.player2.pick[this.player2.index].style.backgroundColor = "silver";
          timesRun = 0;
        }
      }, 2);
      setTimeout(() => {
        this.winner(this.player1.index, this.player2.index);
      }, 200);
    }
  }

  winner(player1, player2) {
    const win = "PLAYER 1\nWIN!";
    const lose = "COM\nWIN!";
    const draw = "DRAW";
    if (player1 == 0 && player2 == 2) {
      this.result.innerHTML = `<p class="win">${win}</p>`;
    } else if (player1 == 2 && player2 == 0) {
      this.result.innerHTML = `<p class="win">${lose}</p>`;
    } else if (player1 > player2) {
      this.result.innerHTML = `<p class="win">${win}</p>`;
    } else if (player1 < player2) {
      this.result.innerHTML = `<p class="win">${lose}</p>`;
    } else {
      this.result.innerHTML = `<p class="draw">${draw}</p>`;
    }
  }
}

// Initialize Player
const player1 = new Human({
  name: "Player 1",
  pick: document.getElementsByClassName("player1Pick"),
});

const computer = new Computer({
  name: "Computer",
  pick: document.getElementsByClassName("computerPick"),
});

// Initialize Game
const game1 = new Game({
  result: document.getElementById("versus"),
  player1: player1,
  player2: computer,
});

// Main Program
let pickStatus = false;

const refresh = () => {
  if (!pickStatus) {
    alert("Pick your choice first!");
  } else {
    game1.startGame();
  }
};

const playerPick = (choice) => {
  player1.index = choice;
  game1.result.innerHTML = `<p class="vs">VS</p>`;
  for (let i = 0; i < 3; i++) {
    computer.pick[i].style.backgroundColor = "#9c835f";
  }
  player1.pick[choice].style.backgroundColor = "silver";
  for (let i = 0; i < 3; i++) {
    if (i !== choice) {
      player1.pick[i].style.backgroundColor = "#9c835f";
    }
  }
  pickStatus = true;
  game1.startGame();
};
