const express = require("express");
const session = require("express-session");
const flash = require("express-flash");
const passport = require("./lib/passport");
const passportJwt = require("./lib/passport-jwt");
const app = express();
const { port = 3000 } = process.env;
const router = require("./routers/router");

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(passportJwt.initialize());

app.use(
  session({
    secret: "Buat ini jadi rahasia",
    resave: false,
    saveUninitialized: false,
  })
);

app.use(flash());

app.use(passport.initialize());
app.use(passport.session());

app.set("view engine", "ejs");

app.use(router);

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
